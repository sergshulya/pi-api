const systemInfo = require('systeminformation')

const PiInfo = function() {}

PiInfo.prototype.getInfo = function() {
  const SERVICES = 'node,mongod,apache2,nginx,java'
  const GOOGLE = 'www.google.com.ua'

  return new Promise((resolve, reject) => {
    const info = { time: systemInfo.time() }

    systemInfo.osInfo()
    .then(os => {
      info.os = {
        distro: os.distro,
        release: os.release,
        kernel: os.kernel
      }
      return systemInfo.cpuCurrentspeed()
    })
    .then(cpuSpeed => {
      info.cpu = cpuSpeed
      return systemInfo.cpuTemperature()
    })
    .then(cpuTemp => {
      info.temperature = { main: cpuTemp.main, max: cpuTemp.max }
      return systemInfo.mem()
    })
    .then(mem => {
      info.memory = {
        total: mem.total,
        free: mem.free,
        used: mem.used,
        swapTotal: mem.swaptotal,
        swapFree: mem.swapfree,
        swapUsed: mem.swapused
      }
      return systemInfo.fsSize()
    })
    .then(fs => {
      info.fileSystem = {
        type: fs[0].type,
        use: fs[0].use,
        mountPoint: fs[0].mount
      }
      return systemInfo.disksIO()
    })
    .then(io => {
      info.io = {
        readSpeed: io.rIO_sec,
        writeSpeed: io.wIO_sec
      }
      return systemInfo.networkStats()
    })
    .then(net => {
      info.network = {
        state: net.operstate,
        downSpeed: net.rx_sec,
        upSpeed: net.tx_sec
      }
      return systemInfo.inetLatency(GOOGLE)
    })
    .then(latency => {
      info.network.latency = latency
      return systemInfo.currentLoad()
    })
    .then(load => {
      info.systemLoad = {
        average: load.avgload,
        current: load.currentload,
        cpuLoad: load.cpus
      }

      return systemInfo.services(SERVICES)
    })
    .then(services => {
      info.services = services

      resolve(info)
    })
    .catch(err => reject(err))
  })
}

module.exports = new PiInfo()
