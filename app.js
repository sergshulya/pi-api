const express = require('express')
const bodyParser = require('body-parser')
const piInfo = require('./API/PiInfo')

const app = express()

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended: true}))

const port = process.env.PORT || 8080

const router = express.Router()

router.use(function(req, res, next) {
  console.log('Pi info requested.')
  next()
})

router.get('/', (req, res) => {
  piInfo.getInfo()
  .then(info => res.json(info))
  .catch(err => res.json({ error: err }))
})

app.use('/api/systemInfo', router)

app.listen(port)
console.log(`Pi API started listening on ${port}`)
